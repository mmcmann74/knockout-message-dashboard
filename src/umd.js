(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "jquery"], function(exports, jquery) {
            return factory(exports, jquery)
        });
    } else if (typeof exports !== "undefined") {
        factory(exports, require('jquery'));
    } else {
        var mod = { exports: {} },
            theModule = factory(mod.exports, jQuery),
            root = typeof exports === 'object' ? exports : global;
        global.actual = mod.exports;
        root[theModule.name] = theModule;
    }
})(this, function (exports2, $) {
    "use strict";
    
    Object.defineProperty(exports2, "__esModule", {
        value: true
    });

    console.log(typeof $);

    var Safelite = {
        name: "Safelite",
    };

    function createModuleProperties(exports, name, namespace) {
        var newModule = {};
        // Module.name string property
        newModule.name = name + '.' + namespace;
        // Module.onModuleReady array property
        newModule.onModuleReady = [];
        // Module.ready function
        newModule.ready = function () { };

        exports.default = newModule;
        // Returned new component as an object.
        return newModule;
    }

    /**
     * @function createNamespace
     * @param namespace String
     * 
     * Create a new UMD module based on the provided namespace.
     */
    Safelite.createNamespace = function (namespace) {
        var ns = namespace;
        var current = this;
        var theModule = this;

        var hasDefine = typeof define === 'function' && define.amd,
            hasExports = typeof module !== 'undefined' && module.exports;
        
        if (hasDefine) {
            define(["exports"], function(exports) {
                return createModuleProperties(exports, this.name, namespace)
            });
        } else if (hasExports) {
            createModuleProperties(exports, this.name, namespace);
        } else {
            // Parse the namespace and create an object
            if (ns.indexOf(".") > -1) {
                var argNs = ns.split(".");
                for (var i = 0; i < argNs.length; i++) {
                    current[argNs[i]] = current[argNs[i]] || { init: (function(j) { return function() { console.log(argNs[j] + " init()")}})(i) };
                    current = current[argNs[i]];
                    // Add each object in namespace to exports in a
                    // flat fashion.
                    // if (hasExports && i !== argsNs.length - 1) {
                        //     exports[argNs[i]] = current;
                        // }
                }
            } else {
                current[ns] = current[ns] || { init: function() { console.log(ns + " init()")} };
                current = current[ns];
                //if (hasExports) exports[argNs[i]] = current;
            }
            var mod = { exports: {} };
            theModule = $.extend(current, createModuleProperties(mod.exports, this.name, namespace));
        }
        return theModule;
    };

    Safelite.Init = function (options) {
        var settings = {
            method: 'init',
            master: '',
            view: '',
            controls: []
        };
        var methods = {
            init: function () {
                if (options) {
                    $.extend(settings, options);
                }
                if (settings.method === 'init') {
                    methods.global();
                    methods.runtime();
                }
                else {
                    methods[settings.method].apply();
                }
            },

            global: function () {
                if (Safelite.Common) {
                    Safelite.Common.init();
                }
            },
            runtime: function () {
                //if necessary, run more specific init code here
                if (settings.master && module[settings.master]) {
                    module[settings.master].init();
                }
                if (settings.view && module[settings.view]) {
                    module[settings.view].init();
                }
                $.each(settings.controls, function (index, value) {
                    if (module[value]) {
                        module[value].init();
                    }
                });
            },
            postInit: function () {
                //hook for other modules to add postInit functions which will run after all other init code
                while (Safelite.PostInitFunctions.length) {
                    Safelite.PostInitFunctions.shift().call();
                }
            }
        };
        methods.init();
    };

    Safelite.PostInitFunctions = [];

    $(document).ready(function () {
        Safelite.master = $('body').data('master') || "";
        Safelite.view = $('body').data('view') || "";
        Safelite.Init({
            master: Safelite.master,
            view: Safelite.view,
            controls: Safelite.controls
        });
        Safelite.Init({
            method: 'postInit'
        });
    });
    
    exports2.default = Safelite;

    return Safelite;
});