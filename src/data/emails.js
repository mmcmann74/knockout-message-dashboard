export const folders = [
  {
    label: 'Current'
  }, {
    label: 'Past'
  },
];

export const emails = [
  {
    subject: 'Appointment Receipt',
    date: '2017-09-01',
    attachments: ['PDF', 'DOC'],
  }, {
    subject: 'Appointment Reminder',
    date: '2017-09-04',
    tasks: [],
  },
];

