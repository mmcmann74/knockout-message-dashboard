// Module/Plugin core
// Note: the wrapper code you see around the module is what enables
// us to support multiple module formats and specifications by 
// mapping the arguments defined to what a specific format expects
// to be present. Our actual module functionality is defined lower 
// down, where a named module and exports are demonstrated. 

// ; (function (name, definition) {
//     var theModule = definition(),
//         // this is considered "safe":
//         hasDefine = typeof define === 'function' && define.amd,
//         // hasDefine = typeof define === 'function',
//         hasExports = typeof module !== 'undefined' && module.exports;

//     if (hasDefine) { // AMD Module
//         define(theModule);
//     } else if (hasExports) { // Node.js Module
//         module.exports = theModule;
//     } else { // Assign to common namespaces or simply the global object (window)
//         this[name] = theModule;
//     }
// })('Common', function () {
//     var module = this;
//     module.init = function () {
//         console.log("common init()");
//     };
//     module.showGlobalMessage = function (msg) {
//         console.log(msg);
//     }
//     return module;
// });

// ----------------------------------------------------------------------------
window.Safelite = {};

function register(name, definition) {
    var theModule = definition(),
        hasDefine = typeof define === 'function',
        hasExports = typeof module !== 'undefined' && module.exports;

    if ( hasDefine ) { // AMD Module
        define(theModule);
    } else if ( hasExports ) { // Node.js Module
        module.exports = theModule;
    } else { // Assign to common namespaces or simply the global object (window)
    
        // account for for flat-file/global module extensions
        var obj = null;
        var namespaces = name.split(".");
        var scope = (this.jQuery || this.ender || this.$ || this);
        for (var i = 0; i < namespaces.length; i++) {
            var packageName = namespaces[i];
            if (obj && i == namespaces.length - 1) {
                obj[packageName] = theModule;
            } else if (typeof scope[packageName] === "undefined") {
                scope[packageName] = {};
            }
            obj = scope[packageName];
        }
    
    }
}

function common() {
        
    var module = this || {};
    
    module.init = function () {
        console.log("common init()");
    };

    module.showGlobalMessage = function (msg) {
        console.log(msg);
    }

    return module;

}

register('Common', common);