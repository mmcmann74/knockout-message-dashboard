import { connect } from 'knockout-store';

function projectListViewModel(params) {
    const vm = {};
    vm.projects = params.projects;
    vm.projectClicked = (selectedProject) => {
        params.selectedProject(selectedProject);
    };
    return vm;
}

function mapStateToParams({ selectedProject, projects }) {
    return { selectedProject, projects };
}

export default connect(mapStateToParams)(projectListViewModel);
