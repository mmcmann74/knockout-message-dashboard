/**
 *
 * emailList view model
 *
 */


import { computed } from 'knockout';
import { connect } from 'knockout-store';
import styles from './email-list.style.less';

function emailListViewModel(params) {
    const vm = {};
    vm.emails = computed(function() {
        const ret = params.emails().filter((email) => {
            if (params.selectedFolder().label === "Current") {
                return email.current;
            } else if (params.selectedFolder().label === "Past") {
                return !email.current;
            }
            return false;
        });
        return ret;
    });
    vm.emailClicked = (selectedEmail) => {
        params.selectedEmail(selectedEmail);
    };
    vm.styles = styles;
    return vm;
}

function mapStateToParams({ selectedEmail, emails, selectedFolder, one }) {
    return { selectedEmail, emails, selectedFolder, two: one.two };
}

/**
 * connect(mapStateToParams, [mergeParams])
 * 
 * Connects a view model to the app state. Pass the view model to be connected to
 * the result of this function.
 * 
 * Arguments
 * 
 * - [mapStateToParams(state, [ownParams]): stateParams] (Function): This argument is
 *   a function to map from the app state (state) to the stateParams object passed to
 *   mergeParams. state will be the value of the observable returned by
 *   getState().
 * 
 * - [mergeParams(stateParams, ownParams): params] (Function): If specified, this
 *   argument is a function responsible for merging stateParams (the result of
 *   mapStateToParams, see above) and ownParams (the params object the connected view
 *   model was called with). If this argument is not specified, Object.assign({},
 *   ownParams, statePrams) is used instead.
 */
export default connect(mapStateToParams)(emailListViewModel);