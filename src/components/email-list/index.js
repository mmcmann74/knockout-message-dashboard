import ko from 'knockout';
import viewModel from './email-list.viewmodel';
import template from './email-list.templ.html';

ko.components.register('email-list', { viewModel, template });
