/**
 *
 * folderList view model
 *
 */

import ko from 'knockout';
import { connect } from 'knockout-store';

function folderListViewModel(params) {
    const vm = {};
    vm.folders = params.folders;
    vm.emailCount = (folder) => {
        const ret = params.emails().filter((email) => {
            if (folder.label === "Current") {
                return email.current;
            } else if (folder.label === "Past") {
                return !email.current;
            }
            return false;
        }).length;
        return ret;
    };
    vm.type = ko.computed(function() { return this.constructor.name}, vm.folders());
    vm.folderClicked = (selectedFolder) => {
        params.selectedFolder(selectedFolder);
    };
    return vm;
}

function mapStateToParams({ folders, selectedFolder, emails }) {
    return { folders, selectedFolder, emails };
}

/**
 * connect(mapStateToParams, [mergeParams])
 * 
 * Connects a view model to the app state. Pass the view model to be connected to
 * the result of this function.
 * 
 * Arguments
 * 
 * - [mapStateToParams(state, [ownParams]): stateParams] (Function): This argument is
 *   a function to map from the app state (state) to the stateParams object passed to
 *   mergeParams. state will be the value of the observable returned by
 *   getState().
 * 
 * - [mergeParams(stateParams, ownParams): params] (Function): If specified, this
 *   argument is a function responsible for merging stateParams (the result of
 *   mapStateToParams, see above) and ownParams (the params object the connected view
 *   model was called with). If this argument is not specified, Object.assign({},
 *   ownParams, statePrams) is used instead.
 */
export default connect(mapStateToParams)(folderListViewModel);