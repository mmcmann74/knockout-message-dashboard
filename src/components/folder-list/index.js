import ko from 'knockout';
import viewModel from './folder-list.viewmodel';
import template from './folder-list.templ.html';

ko.components.register('folder-list', { viewModel, template });
