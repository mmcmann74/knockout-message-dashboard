import ko from 'knockout';
import { setState } from 'knockout-store';
import { folders, emails } from '../../data/emails';
import '../../main.less';
import template from './message-dashboard.template.html';
import viewModel from './message-dashboard.viewmodel.js';
import './message-dashboard.styles.less';
import 'folder-list';
import 'email-list';

ko.components.register('app', { template, viewModel });

console.log("heiloo");
ko.deferUpdates = true;

const state = {
    folders: ko.observableArray(folders),
    selectedFolder: ko.observable(folders[0]),
    emails: ko.observableArray([]),
    selectedEmail: ko.observable(),
    one: {
        two: "TWO"
    }
};

setState(state);

// fake a data fetch
setTimeout(() => {
    let i = 0;
    const observableEmails = emails.map(({ subject, date, atttachments }) => {
        return {
            subject: ko.observable(subject),
            date: ko.observable(date),
            atttachments: ko.observableArray(atttachments),
            current: i++ % 2 === 0
        };
    });
    state.emails(observableEmails);
}, 2000);

ko.applyBindings();

console.log("Hello from message dashboard");