(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "jquery"], function (exports, jquery) {
            return factory(exports, jquery)
        });
    } else if (typeof exports !== "undefined") {
        factory(exports, require('jquery'));
    } else {
        var mod = { exports: {} },
            theModule = factory(mod.exports, jQuery),
            root = typeof exports === 'object' ? exports : global;
        global.actual = mod.exports;
        root[theModule.name] = theModule;
    }
})(this, function (exports2, $) {
    "use strict";

    var hasDefine = typeof define === 'function' && define.amd,
        hasExports = typeof module !== 'undefined' && module.exports;

    Object.defineProperty(exports2, "__esModule", {
        value: hasDefine || hasExports
    });

    // This enables us to store all of our javascript within
    // a root namespace called 'Safelite', for easier
    // testing and the prevention of naming conflicts.
    var Safelite = {};

    Safelite.name = 'Safelite';
    Safelite.onModuleReady = [];
    Safelite.controls = [];
    Safelite.ready = function () {
        Safelite.onModuleReady.each(function (item) {
            item.removeAttr('disabled');
        });
    };

    function createModuleProperties(exports, name, namespace) {
        var newModule = {};
        Object.defineProperty(exports, "__esModule", {
            value: hasDefine || hasExports
        });
        // Module.name string property
        newModule.name = name + '.' + namespace;
        // Module.onModuleReady array property
        newModule.onModuleReady = [];
        // Module.ready function
        newModule.ready = function () { };

        $(document).ready(function() {
            if (typeof newModule.init === "function") {
                newModule.init();
            }
        });

        exports.default = newModule;
        // Returned new component as an object.
        return newModule;
    }

    /**
     * @function createNamespace
     * @param namespace String
     * 
     * Create a new UMD module based on the provided namespace.
     */
    Safelite.createNamespace = function (namespace) {
        var ns = namespace;
        var current = this;
        var theModule = this;

        if (hasDefine) {
            define(["exports"], function (exports) {
                theModule = createModuleProperties(exports, theModule.name, namespace);
            });
        } else if (hasExports) {
            createModuleProperties(exports, this.name, namespace);
        } else {
            // Parse the namespace and create an object
            if (ns.indexOf(".") > -1) {
                var argNs = ns.split(".");
                for (var i = 0; i < argNs.length; i++) {
                    current[argNs[i]] = current[argNs[i]] || {};
                    current = current[argNs[i]];
                    // Add each object in namespace to exports in a flat fashion.
                    // if (hasExports && i !== argsNs.length - 1) {
                    //     exports[argNs[i]] = current;
                    // }
                }
            } else {
                current[ns] = current[ns] || {};
                current = current[ns];
                //if (hasExports) exports[argNs[i]] = current;
            }
            var mod = { exports: {} };
            theModule = $.extend(current, createModuleProperties(mod.exports, this.name, namespace));
        }
        return theModule;
    };

    Safelite.Init = function (options) {
        var settings = {
            method: 'init',
            master: '',
            view: '',
            controls: []
        };
        var methods = {
            init: function () {
                if (options) {
                    $.extend(settings, options);
                }
                if (settings.method === 'init') {
                    methods.global();
                    methods.runtime();
                }
                else {
                    methods[settings.method].apply();
                }
            },

            global: function () {
                if (Safelite.Common) {
                    Safelite.Common.init();
                }
            },
            runtime: function () {
                //if necessary, run more specific init code here
                if (settings.master && module[settings.master]) {
                    module[settings.master].init();
                }
                if (settings.view && module[settings.view]) {
                    module[settings.view].init();
                }
                $.each(settings.controls, function (index, value) {
                    if (module[value]) {
                        module[value].init();
                    }
                });
            },
            postInit: function () {
                //hook for other modules to add postInit functions which will run after all other init code
                while (Safelite.PostInitFunctions.length) {
                    Safelite.PostInitFunctions.shift().call();
                }
            }
        };
        methods.init();
    };

    Safelite.PostInitFunctions = [];

    $(document).ready(function () {
        Safelite.master = $('body').data('master') || "";
        Safelite.view = $('body').data('view') || "";
        Safelite.Init({
            master: Safelite.master,
            view: Safelite.view,
            controls: Safelite.controls
        });
        Safelite.Init({
            method: 'postInit'
        });
    });

    exports2.default = Safelite;

    return Safelite;
});

