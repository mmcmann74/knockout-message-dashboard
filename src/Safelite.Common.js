if (typeof define === "function" && define.amd) {
    define(["exports", "./Safelite", "jquery", "knockout", "modernizr", "bootstrap", "jquery.validate"], function (exports, Safelite, $, ko, Modernizr) {
        factory(exports, Safelite.createNamespace("Common"), $, ko, Modernizr);
    });
} else if (typeof exports !== "undefined") {
    var Safelite = require('./Safelite');
    var ko = require('knockout');
    var $ = require('jquery');
    var Modernizr = require('modernizr');
    require('jquery.validate');
    require('bootstrap');
    factory(exports, Safelite.createNamespace("Common"), $, ko, Modernizr);
} else {
    factory(undefined, Safelite.createNamespace("Common"), $, ko, Modernizr);
}

function factory(exports2, module, $) {
    //#region Public API Definitions

    // This is a global init method!!
    // It will run on EVERY page that includes Safelite.js
    // and Safelite.Common.js.
    module.init = function () {
        console.log("factory.common init()");
    };

    module.a = function(x) {
        console.log("Message of a: " + x);
    }

    module.showGlobalMessage = function (msg) {
        console.log(msg);
    }

    if (exports2) exports2.default = module;
    
    return module;
}
// import s from './Safelite';

// (function (module, $) {

//     //#region Public API Definitions

//     //This is a global init method!!
//     //It will run on EVERY page that includes Safelite.js and Safelite.Common.js.
//     module.init = function () {
//         console.log("common init()");
//     };

//     module.a = function(x) {
//         console.log("common " + x);
//     }

//     module.showGlobalMessage = function (msg) {
//         console.log(msg);
//     }

// }(Safelite.createNamespace("Common"), $));

// (function (module, $) {

//     //#region Public API Definitions

//     //This is a global init method!!
//     //It will run on EVERY page that includes Safelite.js and Safelite.Common.js.
//     module.init = function () {
//         console.log("common utils init()");
//     };

//     module.a = function(x) {
//         console.log("common utils " + x);
//     }

// }(Safelite.createNamespace("Common.Utilities"), $));

// export const a = function(x) {
//     console.log("common " + x);
// }