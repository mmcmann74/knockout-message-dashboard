const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const vendorCss = new ExtractTextPlugin('css/[name]-vendor.css');
const appCss = new ExtractTextPlugin('css/[name].css');
const legacyCss = new ExtractTextPlugin('css/[name].css');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const concat = require("lodash").concat;
const CSS_REGEX = /\.css$|\.less$/;
const CSS_SOURCEMAPS = true;
const IS_DEV = true; // TODO: Import from config


const autoPrefixerOptions = {
  browsers: [
    '>2%',
    'last 4 versions',
    'Firefox ESR',
    'not ie < 9', // React doesn't support IE8 anyway
  ],
  flexbox: 'no-2009',
};

const postCssOptions = {
  // Necessary for external CSS imports to work
  // https://github.com/facebookincubator/create-react-app/issues/2677
  ident: 'postcss',
  sourceMap: IS_DEV,
  plugins: [
    // require('postcss-flexbugs-fixes'),
    autoprefixer(autoPrefixerOptions),
  ],
};

function isVendor({ resource }) {
  return resource &&
    resource.indexOf('node_modules') >= 0 &&
    resource.match(/\.(js|json)$/);
}

const config = {
  entry: {
    // 'legacy': './SafeliteDotcom/css/legacy.less',
    'message-center': [
      // Bundle the client for webpack-dev-server and connect to the provided
      // endpoint
      'webpack-dev-server/client?http://localhost:8080',
      // Bundle the client for hot reloading only- means to only hot reload for
      // successful updates
      'webpack/hot/only-dev-server',
      // Entry points
      './src/containers/message-dashboard/index.js',
    ],
    // 'common-test': [
    //   path.join(process.cwd(), './src/main.js'),
    // ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
    libraryTarget: 'umd',
  },
  // externals: {
  //   'jquery': {
  //     root: '$',
  //     commonjs2: 'jquery',
  //     commonjs: 'jquery',
  //     amd: 'jquery'
  //   }
  // },
  module: {
    rules: [
      // ** BEGIN: ADDING/UPDATING RULES **
      // The `url` loader handles all assets unless explicitly excluded.
      // The `exclude` list *must* be updated with every change to loader extensions.
      // When adding a new loader, you must add its `test` as a new entry in the
      // `exclude` list for "url" loader.

      // `url` loader embeds assets smaller than specified size as data URLs to
      // avoid requests. Otherwise, it acts like the "file" loader.
      {
        exclude: [
          /\.html$/,
          /\.ejs$/,
          /\.(js|jsx)$/,
          CSS_REGEX,
          /\.json$/,
          /\.bmp$/,
          /\.gif$/,
          /\.jpe?g$/,
          /\.png$/,
          /\.svg$/,
        ],
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'media/[name].[hash:8].[ext]',
          },
        }],
      }, {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      /**
       * - `style-loader` turns CSS into JS modules that inject <style> tags.
       * - `css-loader` resolves paths in CSS and adds assets as dependencies.
       * - `postcss-loader` applies autoprefixer to our CSS.
       * - In production, we use ExtractTextPlugin to extract that CSS to a file
       * - In development, `style-loader` enables hot editing of CSS
       * Loader value format is taken from
       * https://medium.com/@lokhmakov/best-way-to-create-npm-packages-with-create-react-app-b24dd449c354
       * I have separated global styles in `index.less` file and extracted
       * it with different loader. I set a different loader for modular CSS from
       * components folder where React component are placed.
       * TODO: https://github.com/gajus/react-css-modules
       */
      {
        test: /\.css$/,
        loader: appCss.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: true,
                sourceMap: IS_DEV,
                camelCase: true,
                localIdentName: '[name]_[local]__[hash:base64:5]',
              },
            }, {
              loader: 'postcss-loader',
              options: postCssOptions,
            },
          ],
        }),
      }, {
        test: /\.less$/,
        exclude: /main\.less$/,
        loader: appCss.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: true,
                sourceMap: IS_DEV,
                camelCase: true,
                localIdentName: '[name]_[local]__[hash:base64:5]',
              },
            }, {
              loader: 'postcss-loader',
              options: postCssOptions,
            }, {
              // This is needed because of the way that less-loader resolves
              // urls. For example, in Logo.less, to include the logo.svg file,
              // you would normally use url('./logo.svg') because the SVG file
              // is in the same directory as the LESS file that is referencing
              // it. However, when this css build happens, less-loader tries to
              // resolve url references relative to the starting point (i.e.,
              // the root less file, `App.less` below) instead of using the
              // current file that's being processed. In other words, the
              // less-loader generates a single css file from multiple less
              // files. That's why the css-loader tries to resolve all urls
              // relative from the less entry file `App.less`.
              //
              // See: https://github.com/webpack-contrib/less-loader/issues/76
              //
              // src/components/
              // |-- Logo/
              // |   |-- Logo.less
              // |   `-- logo.svg
              // |-- Header/
              // |   `-- Header.less
              // `-- App.less
              //
              // To get this to work without using resolve-url-loader, you would
              // have to use url('./Logo/logo.svg') in Logo.less because that's
              // the relative URL from the less entry file `App.less`. Which is
              // confusing and dumb.
              loader: 'resolve-url-loader',
            }, {
              loader: 'less-loader',
              options: {
                sourceMap: IS_DEV,
              },
            },
          ],
        }),
      }, {
        test: /main\.less$/,
        use: vendorCss.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: true,
                sourceMap: true,
                modules: false,
              },
            }, {
              loader: 'postcss-loader',
              options: postCssOptions,
            }, {
              loader: 'resolve-url-loader',
            }, {
              loader: 'less-loader',
              options: {
                sourceMap: CSS_SOURCEMAPS,
              },
            },
          ],
        }),
      }, {
        // JSON is not enabled by default in Webpack but both Node and Browserify
        // allow it implicitly so we also enable it.
        test: /\.json$/,
        use: ['json-loader'],
      }, {
        // "url" loader works like "file" loader except that it embeds assets
        // smaller than specified limit in bytes as data URLs to avoid requests.
        // A missing `test` is equivalent to a match.
        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        use: [{
          loader: require.resolve('url-loader'),
          options: {
            limit: 10000,
            name: 'static/media/[name].[hash:8].[ext]',
          },
        }],
      }, {
        test: /\.svg$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/svg+xml',
            name: 'images/[name].[hash:8].[ext]',
          },
        }],
      }, {
        test: /\.html$/,
        use: [{
          loader: 'html-loader',
          options: {
            minimize : false
          }
        }]
      }
      // ** END: ADDING/UPDATING RULES **
      // Are you adding a new rule? Remember to add the new extension(s) to the
      // "url" loader exclusion list.
    ],
  },
  target: 'web', // Make web variables accessible to webpack, e.g. window
  resolve: {
    modules: ['node_modules', 'src/components'],
    // These are the reasonable defaults supported by the Node ecosystem. We
    // also include JSX as a common component filename extension to support some
    // tools, although we do not recommend using it, see:
    // https://github.com/facebookincubator/create-react-app/issues/290
    extensions: ['*', '.js', '.jsx'],
    alias: {
      jquery: "jquery/src/jquery",
      modernizr: path.resolve(__dirname, "./node_modules/modernizr/bin/modernizr"),
      'jquery.validate': path.resolve(__dirname, "./src/jquery.validate.min"),
    }
  },
  plugins: [
    appCss,
    vendorCss,
    // prints more readable module names in the browser console on HMR updates
    new webpack.NamedModulesPlugin(),
    // do not emit compiled assets that include errors
    new webpack.NoEmitOnErrorsPlugin(),
    // Generate an HTML file from a template
    new HtmlWebpackPlugin({
      template: './src/template.ejs',
      inject: 'body',
    }),
    // split code
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks(module) {
        // this assumes your vendor imports exist in the node_modules directory
        const nodeModulesIndex = module.context.indexOf('node_modules');
        // This prevents stylesheet resources with the .css or .scss extension
        // from being moved from their original chunk to the vendor chunk
        if (module.resource && (/^.*\.(css|less)$/).test(module.resource)) {
          return false;
        }
        // if (nodeModulesIndex !== -1) {
        //     console.log("Found a node module");
        // }
        return module.context && nodeModulesIndex !== -1;
      },
    }),
    // CommonChunksPlugin will now extract all the common modules from vendor and main bundles
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest', // But since there are no more common modules between them
      // we end up with just the runtime code included in the manifest file
    }),
    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new webpack.NamedModulesPlugin(),
    // new webpack.ProvidePlugin({
    //   $: "jquery",
    //   jQuery: "jquery",
    //   "window.jQuery": "jquery",
    //   jquery: "jquery",
    //   // Safelite: "./Safelite"
    // })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    host: 'localhost',
    port: 8080,
    historyApiFallback: true, // respond to 404s with index.html
    hot: true, // enable HMR on the server
  },
  // Emit a source map for easier debugging.  It is "cheap" because it doesn't
  // have column mappings, it only maps line numbers.
  devtool: 'cheap-source-map', // 'inline-source-map',
  performance: {
    hints: 'warning',
  },
};

module.exports = config;
