/**
 * Component Generator
 */

/* eslint strict: ["off"] */

'use strict';

const componentExists = require('../utils/componentExists');

module.exports = {
  description: 'Add an unconnected container',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'Page name:',
    default: 'Message Dashboard',
    validate: (value) => {
      if ((/\-/).test(value)) {
        return 'No dashes!';
      }
      if ((/.+/).test(value)) {
        return componentExists(value) ? 'A container with this name already exists!' : true;
      }
      return 'The name is required';
    },
  }],
  actions: (data) => {
    // Generate index.js and index.test.js

    const actions = [{
      type: 'add',
      path: '../../src/containers/{{dashCase name}}/index.js',
      templateFile: './container/index.js.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/containers/{{dashCase name}}/{{dashCase name}}.viewmodel.js',
      templateFile: './container/viewmodel.js.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/containers/{{dashCase name}}/{{dashCase name}}.templ.html',
      templateFile: './container/template.html.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/containers/{{dashCase name}}/{{dashCase name}}.test.js',
      templateFile: './container/test.js.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/containers/{{dashCase name}}/{{dashCase name}}.style.less',
      templateFile: './container/style.less.hbs',
      abortOnFail: true,
    }];

    return actions;
  },
};
