/**
 * Component Generator
 */

/* eslint strict: ["off"] */

'use strict';

const componentExists = require('../utils/componentExists');

module.exports = {
  description: 'Add an unconnected component',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called? Spaces are cool, case doesn\'t matter, no dashes.',
    default: 'Input Group',
    validate: (value) => {
      if ((/\-/).test(value)) {
        return 'No dashes!';
      }
      if ((/.+/).test(value)) {
        return componentExists(value) ? 'A component with this name already exists!' : true;
      }
      return 'The name is required';
    },
  }],
  actions: (data) => {
    // Generate index.js and index.test.js

    const actions = [{
      type: 'add',
      path: '../../src/components/{{dashCase name}}/index.js',
      templateFile: './component/index.js.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/components/{{dashCase name}}/{{dashCase name}}.viewmodel.js',
      templateFile: './component/viewmodel.js.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/components/{{dashCase name}}/{{dashCase name}}.templ.html',
      templateFile: './component/template.html.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/components/{{dashCase name}}/{{dashCase name}}.test.js',
      templateFile: './component/test.js.hbs',
      abortOnFail: true,
    }, {
      type: 'add',
      path: '../../src/components/{{dashCase name}}/{{dashCase name}}.style.less',
      templateFile: './component/style.less.hbs',
      abortOnFail: true,
    }];

    return actions;
  },
};
